いろいろ煩雑な実験場......
==========================
graph TD;
  a-->B;
  a-->C;
  B-->D;
  C-->D;

!!! Note
    ほかにないのかにゃあ.......

```mermaid
 graph TD;
  a-->B;
  a-->C;
  B-->D;
  C-->D; 
```

こんな感じにね?

```mermaid
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail...
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```

```plantuml format="png" classes="uml myDiagram" alt="My super diagram placeholder" title="My super diagram" width="300px" height="300px"
  Goofy ->  MickeyMouse: calls
  Goofy <-- MickeyMouse: responds
```
